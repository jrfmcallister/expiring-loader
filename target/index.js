"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.expiringLoaderFactory = void 0;
const dataloader_1 = __importDefault(require("dataloader"));
function expiringLoaderFactory(loader, ttl_seconds) {
    const wrappedLoader = (key) => __awaiter(this, void 0, void 0, function* () { return wrappedResp(yield loader(key), ttl_seconds); });
    const wrappedDataLoader = dataLoader(wrappedLoader);
    return (key) => __awaiter(this, void 0, void 0, function* () {
        const wrapped = yield wrappedDataLoader.load(key);
        const current_epoch_seconds = new Date().getTime() / 1000;
        if (wrapped.expiry_epoch_seconds < current_epoch_seconds) {
            wrappedDataLoader.clear(key);
            const newWrapped = yield wrappedDataLoader.load(key);
            return newWrapped.value;
        }
        else {
            return wrapped.value;
        }
    });
}
exports.expiringLoaderFactory = expiringLoaderFactory;
const dataLoader = (loader) => {
    const batchLoader = (keys) => Promise.all(keys.map(loader));
    return new dataloader_1.default(batchLoader);
};
const wrappedResp = (value, ttl_seconds) => {
    const current_epoch_seconds = new Date().getTime() / 1000;
    return {
        value: value,
        expiry_epoch_seconds: ttl_seconds + current_epoch_seconds,
    };
};
