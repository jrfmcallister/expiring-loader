import { factory as expiringLoadedFactory } from "../index";
let data: Record<string, string> = {
  chimay: "Chimay Beer",
  grimbergen: "Grimbergen Beer",
  bock: "Bock Beer",
  tripel: "Tripel Beer",
};

describe("LRU Cache", () => {
  let callCount = 0;
  const loader = async (key: string) => {
    callCount++;
    return data[key];
  };

  const expiringLoader = expiringLoadedFactory(loader, {
    ttl_seconds: 9999,
    lru_limit: 2,
  });

  it("Set up cache with 2 calls", async () => {
    const firstResult = await expiringLoader("tripel");
    const secondResult = await expiringLoader("bock");
    expect(firstResult).toBe("Tripel Beer");
    expect(secondResult).toBe("Bock Beer");
    expect(callCount).toBe(2);
  });
  it("Call same 2 and 2 new ones", async () => {
    const firstResult = await expiringLoader("tripel");
    const secondResult = await expiringLoader("bock");
    const thridResult = await expiringLoader("grimbergen");
    const fourthResult = await expiringLoader("chimay");

    expect(firstResult).toBe("Tripel Beer");
    expect(secondResult).toBe("Bock Beer");
    expect(thridResult).toBe("Grimbergen Beer");
    expect(fourthResult).toBe("Chimay Beer");
    expect(callCount).toBe(4);
  });
  it("call fourth as it should be in cache", async () => {
    const fourthResult = await expiringLoader("chimay");
    expect(fourthResult).toBe("Chimay Beer");
    expect(callCount).toBe(4);
  });
  it("retry the same call", async () => {
    const fourthResult = await expiringLoader("chimay");
    expect(fourthResult).toBe("Chimay Beer");
    expect(callCount).toBe(4);
  });
  it("retry first call, as its missing from cache", async () => {
    const firstResult = await expiringLoader("tripel");
    expect(firstResult).toBe("Tripel Beer");
    expect(callCount).toBe(5);
  });
});
