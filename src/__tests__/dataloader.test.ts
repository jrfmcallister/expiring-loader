import { factory as expiringLoadedFactory } from "../index";
let data: Record<string, string> = {
  james: "hello james",
};

describe("Expiring cache", () => {
  describe("Can load data", () => {
    it("loads hello james", async () => {
      const loader = async (key: string) => data[key];
      const expiringLoader = expiringLoadedFactory(loader, {
        ttl_seconds: 9999,
        lru_limit: 9999,
      });
      expect(await expiringLoader("james")).toBe("hello james");
    });
  });
  describe("Can cache loading", () => {
    it("Loads data once, when called once", async () => {
      let callCount = 0;
      const loader = async (key: string) => {
        callCount++;
        return data[key];
      };
      const expiringLoader = expiringLoadedFactory(loader, {
        ttl_seconds: 9999,
        lru_limit: 9999,
      });
      const result = await expiringLoader("james");
      expect(result).toBe("hello james");
      expect(callCount).toBe(1);
    });
    it("Loads data once, when called twice", async () => {
      let callCount = 0;
      const loader = async (key: string) => {
        callCount++;
        return data[key];
      };
      const expiringLoader = expiringLoadedFactory(loader, {
        ttl_seconds: 9999,
        lru_limit: 9999,
      });
      const firstResult = await expiringLoader("james");
      const secondResult = await expiringLoader("james");
      expect(firstResult).toBe("hello james");
      expect(secondResult).toBe("hello james");
      expect(callCount).toBe(1);
    });
  });

  describe("Can expire cache", () => {
    it("Loads data more than once, when ttl has expired", async () => {
      let callCount = 0;
      const loader = async (key: string) => {
        callCount++;
        return data[key];
      };
      const expiringLoader = expiringLoadedFactory(loader, {
        ttl_seconds: 1,
        lru_limit: 9999,
      });
      const firstResult = await expiringLoader("james");
      await new Promise((r) => setTimeout(r, 2000));
      const secondResult = await expiringLoader("james");
      expect(firstResult).toBe("hello james");
      expect(secondResult).toBe("hello james");
      expect(callCount).toBe(2);
    });
    it("Loads data once, when ttl has not expired", async () => {
      //
    });
  });
});
